<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Songcontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/uploadsong', [Songcontroller::class, 'uploadsong']);
Route::get('/displaysong', [Songcontroller::class, 'displaysong']);
Route::post('/deletemusic', [Songcontroller::class, 'deletemusic']);




Route::post('/createplaylist', [Songcontroller::class, 'createplaylist']);
Route::get('/displayplaylist', [Songcontroller::class, 'displayplaylist']);
Route::post('/deleteplaylist', [Songcontroller::class, 'deleteplaylist']);
Route::post('/editplaylist', [Songcontroller::class, 'editplaylist']);



Route::post('/addsongplaylist', [Songcontroller::class, 'addsongplaylist']);
Route::get('/displaysongtoplaylist/{id}', [Songcontroller::class, 'displaysongtoplaylist']);
Route::post('/deletesongfromplaylist', [Songcontroller::class, 'deletesongfromplaylist']);
Route::get('/displaysongnotinplaylist/{id}', [Songcontroller::class, 'displaysongnotinplaylist']);




Route::post('/test', [Songcontroller::class, 'testCon']);





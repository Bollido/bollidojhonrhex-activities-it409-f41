const EventHandling = {
    data() {
        return {
            headerT: "",
            shown: false,
            music: "",
            editpl: "",
            file: [],
            playlists: [],
            song: [],
            playlistindex: null,
            notinplaylist: [],
            current: ""
        }
    },
    methods:{
        changeText() {
            this.header = ""
        },

        button() {
            var dis = this
            var formData = new FormData();
            formData.append('name', this.music);
            axios.post('http://127.0.0.1:8000/createplaylist',
                formData
            ).then(({ data })=> {
                 dis.playlists.push({
                    id: data,
                    name: dis.music

                })
                 dis.music = ""
            }).catch((err)=> {});
           
        },
        editplaylist(){

            var dis = this
            var formData = new FormData();
            formData.append('id', dis.playlists[dis.playlistindex].id);
            formData.append('name', this.editpl);
            axios.post('http://127.0.0.1:8000/editplaylist',
                formData
            ).then(({ data })=> {
                 dis.playlists[dis.playlistindex].name = dis.editpl
                 document.querySelector(".cancel").click()
            }).catch((err)=> {});


        },
        showplaylistsong(i){
            this.current = this.playlists[i].id
            this.playlistindex = i
            var id = this.playlists[i].id
            this.headerT = this.playlists[i].name
            var dis = this
            axios.get('http://127.0.0.1:8000/displaysongtoplaylist/'+ id).then(({ data })=> {
                this.song = []
                for(i=0; i < data.length; i++){
                    dis.song.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.getduration(data[i].length),
                        pid: data[i].pid
                    })

                }
            }).catch((err)=> {});

        },
        notinplaylistsong(i){
            var id = this.playlists[i].id
            var dis = this
            axios.get('http://127.0.0.1:8000/displaysongnotinplaylist/'+ id).then(({ data })=> {
                dis.notinplaylist = []
                for(i=0; i < data.length; i++){
                    dis.notinplaylist.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.getduration(data[i].length)
                    })

                }
            }).catch((err)=> {});


        },
        addmusic(i){
            var song_id = this.notinplaylist[i].id
            var playlist_id = this.playlists[this.playlistindex].id
            var dis = this
            var formData = new FormData();
            formData.append('song_id', song_id);
            formData.append('playlist_id', playlist_id);
            axios.post('http://127.0.0.1:8000/addsongplaylist',
                formData
            ).then(({ data })=> {
                if (dis.current == playlist_id) {
                
                 dis.song.push({
                        id: dis.notinplaylist[i].id,
                        title: dis.notinplaylist[i].title,
                        artist: dis.notinplaylist[i].artist,
                        album: "none",
                        duration: dis.notinplaylist[i].duration,
                        pid: data
                    })
                }
                 dis.notinplaylist.splice(i, 1)
            }).catch((err)=> {});
        },
        saveplaylistindex(i){

            this.playlistindex = i
            this.editpl = this.playlists[i].name

        },
        drop(e){ 
            e.preventDefault()
            this.file=e.dataTransfer.files
        },
        dragover(e){
            e.preventDefault()

        },
        deleteplaylist(i){
            var dis = this
            var id = this.playlists[i].id
            var formData = new FormData();
            formData.append('id', id);
            axios.post('http://127.0.0.1:8000/deleteplaylist',
                formData
            ).then(({ data })=> {
                 dis.playlists.splice(i, 1)
            }).catch((err)=> {});

        },
        deletemusic(i){
            
            var dis = this
            var id = ""
            var link = ""
            if (this.playlistindex == null) {

                id = this.song[i].id
                link = "deletemusic"

            }else{

                id = this.song[i].pid
                link = "deletesongfromplaylist"

            }
            var formData = new FormData();
            formData.append('id', id);
            axios.post('http://127.0.0.1:8000/'+ link,
                formData
            ).then(({ data })=> {
                 dis.song.splice(i, 1)
            }).catch((err)=> {});

        },
        fup(){
            var title="none";
            var artist="none";
            var album="none";
            var duration="";

            var dis=this;
            var fi=this.file;

            var jsmediatags = window.jsmediatags;
            jsmediatags.read(fi[0], {
              onSuccess: function(tag) {

                var ta=tag.tags
                if (ta.title) {
                    title=ta.title
                }
                else{
                    title=fi[0].name
                }
                if (ta.artist) {
                    artist=ta.artist
                }
                if (ta.album) {
                    album=ta.album
                }
                var src = URL.createObjectURL(fi[0]);
                    var audio = document.querySelector("#r");
                    audio.src = src;
                    audio.onloadedmetadata = function () {
                                        
                            
                        var formData = new FormData();
                        formData.append('title', title);
                        formData.append('artist', artist);
                        formData.append('length', audio.duration);
                         formData.append('song', fi[0]);
                        axios.post('http://127.0.0.1:8000/uploadsong',
                            formData,
                            {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }
                
                        ).then(({ data })=> {
                            duration =dis.getduration(audio.duration);
                        dis.song.push({
                            id: data,
                            title: title,
                            artist: artist,
                            album: album,
                            duration: duration
                        })
                        }).catch((err)=> {});

                        
                         document.querySelector(".cancel").click()
                        
                        dis.file = []
                    };
              },
              onError: function(error) {

                console.log(error);
              }
            });

        },
         getduration(duration) {
            // Hours, minutes and seconds
            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;

            // Output like "1:01" or "4:03:59" or "123:03:59"
            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;
            return ret;
        },
        displaysong(){
            this.current = ""
            this.playlistindex = null
            this.headerT = "All Songs"
            var dis = this
            axios.get('http://127.0.0.1:8000/displaysong').then(({ data })=> {
                this.song = []
                for(i=0; i < data.length; i++){
                    dis.song.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.getduration(data[i].length)
                    })
                }
            }).catch((err)=> {});
          
        },
        displayplaylist(){
            var dis = this
            axios.get('http://127.0.0.1:8000/displayplaylist').then(({ data })=> {
                for(i=0; i < data.length; i++){
                    dis.playlists.push({
                        id: data[i].id,
                        name: data[i].name,
                    })
                }
            }).catch((err)=> {});
          
        }
    },
    beforeMount(){
        this.displaysong()
        this.displayplaylist()
    }

}


Vue.createApp(EventHandling).mount('#div')

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongControllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/uploadsong', [SongController::class, 'uploadsong']);
Route::get('/displaysong', [SongController::class, 'displaysong']);
Route::post('/createplaylist', [SongController::class, 'createplaylist']);
Route::get('/displayplaylist', [SongController::class, 'displayplaylist']);
Route::post('/addsongplaylist', [SongController::class, 'addsongplaylist']);

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use App\Models\Playlist;
 use App\Models\Playlist_song;
 use App\Models\Song;

class Songcontroller extends Controller
{
    public function uploadsong(){

    	$song = new Song();
        $song->title = request('title');
        $song->artist = request('artist');
        $song->length = request('length');
        $song->save();
        $link = request('link');
        return redirect($link);
        

    }

    public function displaysong(){

    	$song = Song::all();
        return $song;
    	
    }
    public function createplaylist(){

    	$playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->save();
        $link = request('link');
        return redirect($link);
    	
    }
    public function displayplaylist(){

    	$playlist = Playlist::all();
        return $playlist;
    }
    public function addsongplaylist(){
    	
    	$playlist = new Playlist();
        $playlist->song_id = request('song_id');
        $playlist->playlist_id = request('playlist_id');
        $playlist->save();
        $link = request('link');
        return redirect($link);

    }
}

const EventHandling = {
    data() {
        return {
            headerT: "All Songs",
            shown: false,
            music: "",
            file: [],
            playlists: [],
            song: [
                {
                    title: "some title",
                    artist: "some artist",
                    album: "some album",
                    duration: "3:45"
                },
                {
                    title: "some title",
                    artist: "some artist",
                    album: "some album",
                    duration: "3:45"
                },
                {
                    title: "some title",
                    artist: "some artist",
                    album: "some album",
                    duration: "3:45"
                },
                {
                    title: "some title",
                    artist: "some artist",
                    album: "some album",
                    duration: "3:45"
                }
            ]
        }
    },
    methods:{
        changeText() {
            this.header = "playlists"
        },

        button() {
            this.playlists.push({playlist: this.music})
        },
        drop(e){ 
            e.preventDefault()
            this.file=e.dataTransfer.files
        },
        dragover(e){
            e.preventDefault()

        },
        fup(){
            var title="";
            var artist="";
            var album="";
            var duration="";

            var dis=this;
            var fi=this.file;

            var jsmediatags = window.jsmediatags;
            jsmediatags.read(fi[0], {
              onSuccess: function(tag) {

                var ta=tag.tags
                if (ta.title) {
                    title=ta.title
                }
                else{
                    title=fi[0].name
                }
                if (ta.artist) {
                    artist=ta.artist
                }
                if (ta.album) {
                    album=ta.album
                }
                var src = URL.createObjectURL(fi[0]);
                    var audio = document.querySelector("#r");
                    audio.src = src;
                    audio.onloadedmetadata = function () {
                        
                        
                        duration =dis.getduration(audio.duration);
                        dis.song.push({
                            title: title,
                            artist: artist,
                            album: album,
                            duration: duration
                        })
                         document.querySelector(".cancel").click()
                        
                        dis.file = []
                    };
              },
              onError: function(error) {

                console.log(error);
              }
            });

        },
         getduration(duration) {
            // Hours, minutes and seconds
            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;

            // Output like "1:01" or "4:03:59" or "123:03:59"
            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;
            return ret;
        }
        
    }
}


Vue.createApp(EventHandling).mount('#div')
